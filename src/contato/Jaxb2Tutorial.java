package contato;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class Jaxb2Tutorial {

    public static void main(String args[]) throws JAXBException, FileNotFoundException {

        Endereco endereco1 = new Endereco("Rua Moraes", 40, "Centro", "Rio de Janeiro", "20000-000");
        Endereco endereco2 = new Endereco("Rua dos Arcos", 2000, "Ibirapuera", "Sao Paulo", "11000-000");
        Endereco endereco3 = new Endereco("Av. Rio Branco", 1, "Centro", "Rio de Janeiro", "21000-211");

        //criando o objeto de lista de endereços
        List<Endereco> enderecos = new ArrayList<Endereco>();

        //populando a lista com três endereços
        enderecos.add(endereco1);
        enderecos.add(endereco2);
        enderecos.add(endereco3);

        Contato contato = new Contato("Marco Maciel", "M", 34, enderecos);

        JAXBContext context = JAXBContext.newInstance("contato");

        //saída 1 – console
        Marshaller m = context.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        m.marshal(contato, System.out);

        //saída 2 – arquivo
        File f = new File("src/contatos.xml");
        Marshaller m2 = context.createMarshaller();
        m2.marshal(contato, new FileOutputStream(f));

        Unmarshaller um = context.createUnmarshaller();
        Object obj = um.unmarshal(new File("src/contatos.xml"));
        m.marshal(obj, System.out);

        Contato contato2 = (Contato) obj;
        System.out.println(contato2.getNome() + "-" + contato2.getSexo() + "-" + contato2.getIdade());
        for (Endereco listaDeEndereco : enderecos) {
            System.out.println("--> " + listaDeEndereco.getLogradouro()+" - "+listaDeEndereco.getBairro());
        }

    }
}
